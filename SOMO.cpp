// SOMO II sound module  http://www.4dsystems.com.au/product/SOMO_II/
// Datasheet http://www.4dsystems.com.au/productpages/SOMO-II/downloads/SOMO-II_datasheet_R_1_2.pdf
#include "mbed.h"
#include "SOMO.h"

//constructor
SOMO::SOMO(PinName TXPin, PinName RXPin): _serialSOMO(TXPin,RXPin){
    init();
}


void SOMO::init(){         //configure the mbed for SOMO board
    _serialSOMO.baud(9600);
}

void SOMO::playTrackName(char name[]){ //added so SOMO and smartWAV have same functions and can therefore be used interchangeably
}

/*
mp3 sound files should be placed in "01" folder in the root directory of SD card. The filenames should start with number between 001 and 099
followed by up to 5 characters (the freakin' datasheet says up to 32 characters, don't listen to it, it's a liar and a con artist!)
examples: root/01/001left.mp3, root/01/002right.mp3, root/01/099abcde.mp3
*/
void SOMO::playTrackNum(unsigned char track){  //play specific track number
    unsigned char  play[8] = {0x7E, 0x0F, 0x00, 0x01, track, 0xFF, (240-track), 0xEF};
    for (int i = 0; i <8; i++){
        _serialSOMO.putc(play[i]);
    }
}

void SOMO::stopTrack(){       //Stop playing any active song
    unsigned char  stop[8] = {0x7E, 0x16, 0x00, 0x00, 0x00, 0xFF, 0xEA, 0xEF};
    for (int i = 0; i <8; i++){
      _serialSOMO.putc(stop[i]);
    }
}

void SOMO::volume(unsigned char vol){  //change volume between 0 and 30
    if (vol>0 && vol<31){
        unsigned char  changeVol[8] = {0x7E, 0x06, 0x00, 0x00, vol, 0xFF, (250-vol), 0xEF};
        for (int i = 0; i <8; i++){
            _serialSOMO.putc(changeVol[i]);
        }
    }
}

void SOMO::reset(){   //reset SOMO module
    unsigned char reset[8] = {0x7E, 0x0C, 0x00, 0x00, 0x00, 0xFF, 0xF4, 0xEF};
    for (int i = 0; i <8; i++){
        _serialSOMO.putc(reset[i]);
    }
}
