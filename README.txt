ECU Firmware version 1.2

To update the firmware on the ECU, make sure the ECU is plugged into a Windows machine via USB and turned on. Double-click on flash_bitfile.bat to start update.  The process can take up to 5 minutes.


ADDED FEATURES

Version 1.2
Modified communication with MCU to enable 20kHz operation

