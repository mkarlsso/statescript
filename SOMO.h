// SOMO II sound module  http://www.4dsystems.com.au/product/SOMO_II/
// Datasheet http://www.4dsystems.com.au/productpages/SOMO-II/downloads/SOMO-II_datasheet_R_1_2.pdf

#ifndef SOMO_H
#define SOMO_H

#include <mbed.h>

//**************************************************************************
// class SOMO SOMO.h
// This is the main class. It shoud be used like this : SOMO audio(p13,p14);

class SOMO{

public:

    SOMO(PinName TXPin, PinName RXPin);

    void init();

    void playTrackName(char[]);

    void playTrackNum(unsigned char);

    void stopTrack();

    void volume(unsigned char);

    void reset();


    protected :

    Serial     _serialSOMO;

};

#endif
