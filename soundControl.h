#include "mbed.h"
#include "SMARTWAV.h"
#include <stdint.h>
#include <string.h>
#include <string>
#include <vector>
#include <queue>


class soundControl {
    
public:
    soundControl(void);
    void setFile(string fileNameIn);
    void setVolume(int* volumeIn);
    void setVolume(int volumeIn);
    void setPlayback(bool playIn);
    void setReset();
    void execute();
    
private:
    char fileName[21];
    bool fileNameExists;
    int* volumePtr;
    int volume;
    bool play;
    bool reset;
    
    
};