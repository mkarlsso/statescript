#include <stdio.h>
#include <stdint.h>
#include "registers.h"
#include "ff.h"
#include "diskio.h"

struct fifo {
  uint16_t idx_w;
  uint16_t idx_r;
  uint16_t count;
  uint8_t buff[1024];
};

volatile struct fifo txfifo = { 0, 0, 0, { 0 } };
volatile struct fifo rxfifo = { 0, 0, 0, { 0 } };

uint32_t millis = 0, old_millis = 0, saved_ms = 0;
volatile uint8_t interrupt_flag = 0;



/*
// Polling version
uint8_t inbyte(void)
{
  uint8_t c;
  // wait for RX data
  while ((UART0_UCR & UART_UCR_RX_DATA) == 0);
  c = UART0_UDR;
  return c;
}
*/

uint8_t inbyte(void)
{
  uint8_t c;
  uint16_t i;
  while (rxfifo.count == 0);
  i = rxfifo.idx_r;
  c = rxfifo.buff[i++];
  if(i >= sizeof(rxfifo.buff))
    i = 0;
  rxfifo.idx_r = i;
  // Any operations on count must be done with interrupts disabled
  disable_interrupts();
  rxfifo.count--;
  enable_interrupts();
  return c;
}


/*
// Polling version
void outbyte(uint8_t c)
{
  // wait if TX fifo full
  while ((UART0_UCR & UART_UCR_TX_FULL));
  UART0_UDR = c;
}
*/

void outbyte (uint8_t c) {
  uint16_t i;
  i = txfifo.idx_w;
  while (txfifo.count >= sizeof(txfifo.buff));
  txfifo.buff[i++] = c;
  // Any operations on count must be done with interrupts disabled
  disable_interrupts();
  txfifo.count++;
  enable_interrupts();
  // make sure TX interrupt is enabled
  UART0_UCR |= UART_UCR_TX_IE;
  if(i >= sizeof(txfifo.buff))
    i = 0;
  txfifo.idx_w = i;
}

void msleep(unsigned int ms)
{
	TMR_DELAY = ms * 1000;
	while ((TMR_DCR & TMR_DCR_DLY_TO) == 0);
}

void usleep(unsigned int us)
{
	TMR_DELAY = us;
	while ((TMR_DCR & TMR_DCR_DLY_TO) == 0);
}

unsigned long get_ms()
{
  unsigned int millis = TMR_MILLIS;
  return millis;
}

FATFS Fatfs;

volatile BYTE rtcYear = 2013-1980, rtcMon = 7, rtcMday = 6, rtcHour, rtcMin, rtcSec;

DWORD get_fattime (void)
{
	DWORD tmr;

	/* Pack date and time into a DWORD variable */
	tmr =	  (((DWORD)rtcYear - 80) << 25)
			| ((DWORD)rtcMon << 21)
			| ((DWORD)rtcMday << 16)
			| (WORD)(rtcHour << 11)
			| (WORD)(rtcMin << 5)
			| (WORD)(rtcSec >> 1);

	return tmr;
}

void myISR( void ) __attribute__ ((interrupt_handler));

//----------------------------------------------------
// INTERRUPT HANDLER FUNCTION
//----------------------------------------------------
void myISR( void )
{
  uint32_t status;
  // Check if timer interrupt bit set in interrupt controller
  if (INTC_IPR & TMR_INT) {
    // Yes, timer interrupt
	  status = TMR_DCR;
    if (status & TMR_DCR_MS_IF) {
      // millisec interrupt
      TMR_DCR = (status & 0x7ff) | TMR_DCR_MS_IF; // clear interrupt flag
      millis++;
      // do other things needed every ms
      if (millis - old_millis == 1000) {
        old_millis = millis;
        // grab the hardware millisecond counter for printout
        saved_ms = get_ms();
        interrupt_flag = 1;
        DIO_DOUT ^= 0x80000000;
      }
    } else if (status & TMR_DCR_DLY_IF) {
      // delay interrupt
      TMR_DCR = (status & 0x7ff) | TMR_DCR_DLY_IF; // clear interrupt flag
      // do stuff in response to the delay interrupt
    }
    // Clear timer interrupt in interrupt controller
	  INTC_IAR = TMR_INT;
  } 
  else 
  // Check if UART0 interrupt bit is set in interrupt controller
  if (INTC_IPR & UART0_INT) {
    // Yes, UART0 interrupt
    status = UART0_UCR;
    // First check RX interrupt
    if ((status & (UART_UCR_RX_DATA | UART_UCR_RX_IE)) == (UART_UCR_RX_DATA | UART_UCR_RX_IE)) {
      uint16_t i;
      uint8_t c = UART0_UDR;
      i = rxfifo.idx_w;
      if (rxfifo.count < sizeof(rxfifo.buff)) {
        rxfifo.buff[i++] = c;
        rxfifo.count++;
        if(i >= sizeof(rxfifo.buff))
          i = 0;
        rxfifo.idx_w = i;
      }
    }
    // Next check TX interrupt
    if ((status & (UART_UCR_TX_HALF | UART_UCR_TX_IE)) == UART_UCR_TX_IE) {
      uint16_t n, i;
      n = txfifo.count;
      if (n) {
        txfifo.count = --n;
        i = txfifo.idx_r;
        UART0_UDR = txfifo.buff[i++];
        if(i >= sizeof(txfifo.buff))
          i = 0;
        txfifo.idx_r = i;
      }
      if(n == 0)
        UART0_UCR = UART0_UCR & ~UART_UCR_TX_IE;
    }
    // Clear UART0 interrupt in interrupt controller
	  INTC_IAR = UART0_INT;
  } 
  else 
  // Check if UART1 interrupt bit is set in interrupt controller
  if (INTC_IPR & UART1_INT) {
    // Yes, UART1 interrupt
    status = UART1_UCR;
    if ((status & (UART_UCR_RX_DATA | UART_UCR_RX_IE)) == (UART_UCR_RX_DATA | UART_UCR_RX_IE)) {
      // uart1 rx interrupt
      uint8_t c = UART1_UDR;
      // do something with the received char, like send it back
      UART1_UDR = c;
    } 
    // place code for tx interrupt if needed here
    // Clear UART interrupt in interrupt controller
	  INTC_IAR = UART1_INT;
  }
  else 
  // Check if digital I/O edge interrupt bit is set in interrupt controller
  if (INTC_IPR & DIO_INT) {
    // Yes, DIO edge interrupt
    // First check rising edge bits
    uint32_t edge = (DIO_RINT_FLAG | DIO_RINT_MASK);
    if (edge) {
      // process rising edge bits here
      DIO_RINT_FLAG = edge; // clear edge bits
    }
    // next check falling edge bits
    edge = (DIO_FINT_FLAG | DIO_FINT_MASK);
    if (edge) {
      // process falling edge bits here
      DIO_FINT_FLAG = edge; // clear edge bits
    }
    // Clear DIO interrupt in interrupt controller
	  INTC_IAR = DIO_INT;
  }
  else 
  // Check if SPI0 interrupt bit is set in interrupt controller
  if (INTC_IPR & SPI0_INT) {
    // Handle SPI0 interrupt here
    
    // Clear SPI0 interrupt in interrupt controller
	  INTC_IAR = SPI0_INT;
  }
}



//----------------------------------------------------
// MAIN FUNCTION
//----------------------------------------------------
int main (void)
{
  // Init timer: clocks per microsecond, timer enable, ms interrupt enable
  TMR_DCR = (F_CPU/1000000) | TMR_DCR_ENABLE | TMR_DCR_MS_IE;
  
  // Init UART0
  UART0_UBRR = 53; // 115200  baud
  UART0_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable

  // Init UART1
  UART1_UBRR = 53; // 115200  baud
  UART1_UCR = UART_UCR_ENABLE | UART_UCR_RX_IE; // uart enable, uart rx interrupt enable

  // Init SD-card SPI controller
  SPI0_PORTSET = 1 << SPI_SS; // make CS output
  SPI0_DIRSET = 1 << SPI_SS;  // set CS high
  
  // DIO
  DIO_FINT_MASK = 0x00000000; // disable falling int on all DIO inputs
  DIO_RINT_MASK = 0x00000000; // disable rising edge on all DIO inputs
  DIO_DCR = DIO_DCR_IE;       // enable DIO edge interrupts

  // Clear any pending timer interrupt
  TMR_DCR |= (TMR_DCR_MS_IF | TMR_DCR_DLY_IF);

  // Clear any pending DIO interrupt
  DIO_FINT_FLAG = 0xffffffff;
  DIO_RINT_FLAG = 0xffffffff;

  // Enable timer, uart0, uart1, dio and spi0 interrupts in interrupt controller
  INTC_IER = TMR_INT | UART0_INT | UART1_INT | DIO_INT | SPI0_INT;
  INTC_MER = 0x03; // enable interrupt controller

  // Enable Microblaze interrupts
  enable_interrupts();

  // Try to mount the sd card
  if (disk_initialize(0)) {
		printf("\r\nERROR: Unable to initialize SD card!\r\n");
	} else {
    if (f_mount(&Fatfs, "", 0)) {
      printf("\r\nERROR: Unable to mount file system!\r\n");
    } else {
      printf("\r\nSD-card succesfully mounted!\r\n");
    }
  }
    
  while(1){
    if (interrupt_flag) {
      interrupt_flag = 0;
      printf("Timer interrupt: millis = %d\r\n", saved_ms);
    }
    if (rxfifo.count > 0) {
      // just echo back the character
      outbyte(inbyte());
    }
  }
}
