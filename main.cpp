#include "behave.h"

#ifdef K64HARDWARE
extern uint32_t timeKeeper; //the master clock
extern bool resetTimer;
void tick(){
    timeKeeper++;
    if (resetTimer){
        timeKeeper = 0;
        resetTimer = false;
    }
}
Ticker keeptime;

int main() {
    keeptime.attach_us(&tick,1000);
#else
using namespace std;

int main() {
#endif
    mainLoop m;
    m.init();
    m.exec();
}
