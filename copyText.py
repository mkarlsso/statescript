import pyperclip

fileNames = ['mbedInterface.cpp','mbedInterface.h',
            'behave.cpp','behave.h',
            'hardwareInterface.cpp','hardwareInterface.h',            
            'main.cpp']
print()
for fname in fileNames:
    with open(fname) as text:
        pyperclip.copy(text.read())
        input("{} copied to clipboard.\n-Press Enter to copy next file-\n".format(fname))
print('No more files!')
