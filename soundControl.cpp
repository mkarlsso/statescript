#include "soundControl.h"

extern SMARTWAV sWav;

soundControl::soundControl(void):
fileNameExists(false),
volumePtr(NULL),
volume(-1),
play(true),
reset(false) {
    
}

void soundControl::setFile(string fileNameIn) {
    for (int i = 0; i < 20; i++) {
        fileName[i] = NULL;
    }
    std::size_t length = fileNameIn.size();
    if (length <= 20) {
        fileNameIn.copy(fileName, length, 0);
        fileNameExists = true;
    }
}
void soundControl::setVolume(int volumeIn) {
    
    if ((volumeIn >= 0) && (volumeIn < 256)) {
        volume = volumeIn;
        volumePtr = NULL;
    }
}

void soundControl::setVolume(int* volumeIn) {
    
    volume = -1;
    volumePtr = volumeIn;
    
}

void soundControl::setPlayback(bool playIn) {
    play = playIn;
}

void soundControl::setReset() {
    reset = true;
}

void soundControl::execute() {
    
    if (reset) {
        sWav.reset();
    } else if (!play) {
        sWav.stopTrack();
    } else {
        if (volume > -1) {
            sWav.volume(volume);
        } else if (volumePtr != NULL) {
            sWav.volume(*volumePtr);
        }
        
        if (fileNameExists) {
            //sWav.playTracks();
            sWav.stopTrack();
            sWav.playTrackName(fileName);
            
            
            
        }
    }
}



