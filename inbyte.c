#include "registers.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
char inbyte(void);
#ifdef __cplusplus
}
#endif 

char inbyte(void) {
  uint8_t c;
  // wait if RX fifo data
  while (!(UART0_UCR & UART_UCR_RX_DATA));
  c = UART0_UDR;
  return c;
}
