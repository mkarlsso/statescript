CC=mb-gcc
CPP=mb-g++
LD=mb-gcc

CPPSRC     = main.cpp behave.cpp hardwareInterface.cpp fpgaInterface.cpp
CSRC       = inbyte.c outbyte.c mmc.c ff.c ccsbcs.c
ASRC       = utils.S
CPPOBJ     =  $(CPPSRC:.cpp=.o)
COBJ       =  $(CSRC:.c=.o)
AOBJ       =  $(ASRC:.S=.o)

ELF_OUT    = test.elf
ELF_BIT    = elf.bit
BIT_OUT    = test.bit
LD_SCRIPT  = lpddr.ld

CPPFLAGS=-c -g -Os -w -mlittle-endian -mxl-barrel-shift -mxl-pattern-compare -mcpu=v8.40.b -mno-xl-soft-mul -nostdlib
CFLAGS=-c -g -Os -w -mlittle-endian -mxl-barrel-shift -mxl-pattern-compare -mcpu=v8.40.b -mno-xl-soft-mul
LDFLAGS=-Wl,--no-relax -Wl,-Map,build.map -Wl,-T -Wl,$(LD_SCRIPT) -mlittle-endian

.PHONY: clean

elf: $(ELF_OUT)

flash: $(BIT_OUT)
	./fpgaprog.exe -v -d Spikegadgets\ ECU\ B -f $(BIT_OUT) -b bscan_spi_lx45_csg324.bit -n 180000 -sa -r

flashbit: $(BIT_OUT)
	./fpgaprog.exe -v -d Spikegadgets\ ECU\ B -f $(BIT_OUT) -b bscan_spi_lx45_csg324.bit -sa -r

$(BIT_OUT): $(ELF_BIT) $(ELF_OUT)
	./bitmerge $(ELF_BIT) 180000:$(ELF_OUT) $(BIT_OUT)
  
$(ELF_OUT): $(CPPOBJ) $(COBJ) $(AOBJ) $(LD_SCRIPT)
	$(LD) $(LDFLAGS) -o $(ELF_OUT) $(CPPOBJ) $(COBJ) $(AOBJ) libstdc++.a

$(CPPOBJ) : %.o : %.cpp
	$(CPP) $(CPPFLAGS) $< -o $@ 
  
$(COBJ) : %.o : %.c
	$(CC) $(CFLAGS) $< -o $@ 

$(AOBJ) : %.o : %.S
	$(CC) $(CFLAGS) $< -o $@
  


clean:
	rm -f $(ELF_OUT) $(BINARY) $(CPPOBJ) $(COBJ) $(AOBJ)
